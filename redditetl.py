import praw
import pandas as pd
import pprint
import re
import sys
import textblob

def remove_urls(var_text):
    var_text = re.sub(r'(https|http)?:\/\/(www\.)?snapchat\.com(\w|\.|\/|\?|\=|\&|\%)*\b', 'snapchat_com_link', var_text, flags=re.MULTILINE)
    var_text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', 'other_link', var_text, flags=re.MULTILINE)
    return var_text

def get_reddit(reddit_id, reddit_secret, user_agent):
    reddit = praw.Reddit(client_id=reddit_id,
                         client_secret=reddit_secret,
                         user_agent=user_agent
                         )
    return reddit

def get_submissions_df(subreddit, submissions_limit=10, df_file="./data/df_submissionsa.csv"):
    df_index = 0
    df_submissions = pd.DataFrame()
    for sbm in subreddit.new(limit=submissions_limit):
        try:
            sbm_dict = {'id':sbm.id,
                        'title': (sbm.title or '').encode('utf-8', errors='ignore').decode('utf8'),
                        'ups':sbm.ups,
                        'downs':sbm.downs,
                        'score':sbm.score,
                        'edited':sbm.edited,
                        'hidden':sbm.hidden,
                        'media':len(sbm.media or ''),
                        'is_video':sbm.is_video,
                        'selftext':(sbm.selftext or '').encode('utf-8', errors='ignore').decode('utf8'),
                        'likes':sbm.likes,
                        'url':sbm.url,
                        'flair':(sbm.link_flair_text or 'no_flair').encode('utf-8', errors='ignore').decode('utf8'),
                        'author_id':(sbm.author_id or 'no_author').encode('utf-8', errors='ignore').decode('utf8'),
                        'author':(sbm.author.name or 'no_author').encode('utf-8', errors='ignore').decode('utf8'),
                        'created_utc':sbm.created_utc,
                        'num_comments':sbm.num_comments,
                        'num_crossposts':sbm.num_crossposts,
                        'num_reports':sbm.num_reports,
                        'over_18':sbm.over_18,
                        'pinned':sbm.pinned}
            df_submissions = df_submissions.append(pd.DataFrame(sbm_dict, index=[df_index]))
            df_index += 1
            if df_index % 1000 == 0:
                print("submissions: " + str(df_index))
                # df_submissions.to_csv(df_file)
        except Exception as e:
            print("Submission '" + str(sbm.id) + "' Error: " + str(e))
            # print(pprint.pprint(vars(sbm)))
    return df_submissions

def get_comments_df(reddit, submissions):
    df_index = 0
    df_comments = pd.DataFrame()
    for sbm_id in submissions:
        sbm = reddit.submission(id=sbm_id)
        for comment in sbm.comments:
            try:
                comment_dict = {'id':comment.id,
                                'sbm_id':sbm_id,
                                'body': (comment.body or '').encode('utf-8', errors='ignore').decode('utf8'),
                                'ups':comment.ups,
                                'score':comment.ups - comment.downs,
                                'downs':comment.downs,
                                'author':(comment.author.name or 'no_author').encode('utf-8', errors='ignore').decode('utf8'),
                                'author_id':(comment.author_id or 'no_author').encode('utf-8', errors='ignore').decode('utf8'),
                                'created_utc':comment.created_utc
                                }
                df_comments = df_comments.append(pd.DataFrame(comment_dict, index=[df_index]))
                df_index += 1
                if df_index % 1000 == 0:
                    print("commnets: " + str(df_index))
                    # df_comments.to_csv(df_file)
            except Exception as e:
                print("Comment '" + str(comment.id) + "' Error: " + str(e))
                # print(pprint.pprint(vars(comment)))
    # aggregate links
    df_comments['body'] = df_comments['body'].apply(remove_urls)
    # get comment sentiment and subjectivity
    df_comments['sentiment'] = df_comments['body'].apply(lambda x: textblob.TextBlob(x).polarity)
    df_comments['subjectivity'] = df_comments['body'].apply(lambda x: textblob.TextBlob(x).subjectivity)
    return df_comments

def get_users_df(df_submissions, df_comments):
    # group comments by user
    dfg = df_comments.groupby('author').agg({'author':'count', 
                                         'sbm_id':(lambda x: len(x.unique())),
                                         'sentiment':'mean',
                                         'subjectivity':'mean',
                                         'score':'mean'})
    dfg_com = dfg.rename(index=str, columns={"author": "comments_cnt", 
                                         "sbm_id": "commented_sbms_cnt",
                                         "score": "comments_score_avg",
                                         "sentiment": "sentiment_avg",
                                         "subjectivity": "subjectivity_avg"
                                        })
    # group submissions by user
    dfg = df_submissions.groupby(['author']).agg({'author':'count', 
                                         'num_comments':'sum',
                                         'score':'sum'})
    dfg_sbm = dfg.rename(index=str, columns={"author": "sbm_cnt", 
                                       "num_comments": "total_sbm_comments",
                                       "score":"total_sbm_score"})
    # group submissions by user and falir
    dfg = df_submissions.groupby(['author', 'flair']).agg({'author':'count'})
    dfg_flairs = dfg.unstack('flair').fillna(0)
    dfg_flairs.columns = dfg_flairs.columns.droplevel()
    dfg_flairs.columns = ["f_" + x for x in dfg_flairs.columns]
    # return combined user df
    df_users = pd.concat([pd.concat([dfg_sbm, dfg_flairs], axis=1), dfg_com], axis=1, sort=False)
    return df_users
